# Gitで重いリポジトリをcloneするとき

coreserver上でGitPrepをCGIで動作させた場合に、
重いレポジトリをcloneしようとすると

    remote: Counting objects: 3311, done.
    remote: fatal: unable to create thread: Resource temporarily unavailable
    remote: aborting due to possible repository corruption on the remote side.
    fatal: protocol error: bad pack header

などのようにエラーが出てしまう。


この場合には以下のように行うと成功する。

    git clone --depth=1 http://example.com/fuga.git
    git fetch --depth=10
    git fetch --depth=100 # 値は調整しつつ
    git fetch --unshallow # 最後に全部落とす

（[参考ページ1](http://r-h.hatenablog.com/entry/2013/12/07/093423)
、
[参考ページ2](https://qiita.com/butchi_y/items/cc0fe50acc47c1e3ab32)
）

ただし重いといっても他のサーバでは問題なくcloneできるので
coreserverを使用した時だけの問題かもしれない。

